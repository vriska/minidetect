# packages yay
from __future__ import print_function
import numpy as np
import cv2
import math
import imutils

# shrink image for speed, keep the original
image = cv2.imread("in.png")
ratio = image.shape[0] / 300.0
orig = image.copy()
r = 300.0 / image.shape[0]
dim = (int(image.shape[1] * r), 300)
image = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGBA)

# edge detect w/ blurring to ignore noise
gray = cv2.cvtColor(image, cv2.COLOR_RGBA2GRAY)
gray = cv2.bilateralFilter(gray, 11, 17, 17)
edged = cv2.Canny(gray, 30, 200)

# uncomment for debugging of edge detection (noisy edge of screen can cause a crash, white border recommended)
# cv2.imshow("edged", edged)
# cv2.waitKey(0)

# vectorize edges in image, filter on largest 10
(_, cnts, _) = cv2.findContours(edged.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
cnts = sorted(cnts, key = cv2.contourArea, reverse = True)[:10]
screenCnt = None

# look for large rectangular contours
for c in cnts:
  # "approximate the contour," not sure what this does, but probably helpful
  peri = cv2.arcLength(c, True)
  approx = cv2.approxPolyDP(c, 0.02 * peri, True)
  
  # largest rectangular contour is (probably) the screen
  # this was a weird z-shape once, idk
  if len(approx) == 4:
    screenCnt = approx
    break

# draw the detected screen for debugging, uncomment imshow line if necessary
cv2.drawContours(image, [screenCnt], -1, (0, 255, 0), 3)
# cv2.imshow("screen", image)
# cv2.waitKey(0)

# sort contour
pts = screenCnt.reshape(4, 2)
rect = np.zeros((4, 2), dtype = "float32")
 
# math, or something
s = pts.sum(axis = 1)
rect[0] = pts[np.argmin(s)]
rect[2] = pts[np.argmax(s)]
 
diff = np.diff(pts, axis = 1)
rect[1] = pts[np.argmin(diff)]
rect[3] = pts[np.argmax(diff)]

# expand the rectangle to the original size
rect *= ratio

# find the width of the screen
(tl, tr, br, bl) = rect
widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
 
# height too
heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
 
# final dimensions
maxWidth = max(int(widthA), int(widthB))
maxHeight = max(int(heightA), int(heightB))
 
# math i really don't understand
dst = np.array([
	[0, 0],
	[maxWidth - 1, 0],
	[maxWidth - 1, maxHeight - 1],
	[0, maxHeight - 1]], dtype = "float32")
 
# opencv magic
M = cv2.getPerspectiveTransform(rect, dst)
warp = cv2.warpPerspective(orig, M, (maxWidth, maxHeight))

# write the screen out
cv2.imwrite("screen.png", warp)

# display the screen if necessary
# cv2.imshow("warped", warp)
# cv2.waitKey(0)

# resize both the images to what i believe is half an ipad's (used for testing) resolution
imageA = cv2.resize(warp.copy(), (1074, 818), interpolation = cv2.INTER_AREA)
imageB = cv2.resize(cv2.imread("in2.png"), (1074, 818), interpolation = cv2.INTER_AREA)

# i'd prefer to blur them as a fast, hacky noise removal; actual noise removal is slow, but works much better (more testing needed, currently using actual, slow filter)
blurFunc = lambda img: cv2.bilateralFilter(img, 11, 17, 17)
blurredA = blurFunc(imageA)
blurredB = blurFunc(imageB)

# edge detection
edgedA = cv2.Canny(blurredA, 30, 200)
edgedB = cv2.Canny(blurredB, 30, 200)

# debugging
#cv2.imshow("edges", edgedA)
#cv2.waitKey(0)

# find circular edges
circlesA = cv2.HoughCircles(edgedA, cv2.HOUGH_GRADIENT, 1.2, 150, param2=125)
circlesB = cv2.HoughCircles(edgedB, cv2.HOUGH_GRADIENT, 1.2, 150, param2=125)

# make canvases for debugging
outputA = imageA.copy()
outputB = imageB.copy()

if circlesA is not None:
  # integers are good
  circlesA = np.round(circlesA[0, :]).astype("int")

if circlesB is not None:
  circlesB = np.round(circlesB[0, :]).astype("int")
 
  # loop over the (x, y) coordinates and radius of the circlesB
  for (x, y, r) in circlesB:
    # draw the circle in the outputB edgedB, then draw a rectangle
    # corresponding to the center of the circle
    if r < 100 and r > 50:
      cv2.circle(outputB, (x, y), r, (0, 255, 0), 4)
      cv2.rectangle(outputB, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)
 
  # debugging of circles in 2nd image
  # cv2.imshow("outputB", outputB)
  # cv2.waitKey(0)

# check if a ring is a solid color (in case of a phantom circle)
def ring_solid_color(circle, image):
  x, y, r = circle
  pixels_checked = []

  # initialize mask to sort through for points along circle
  mask = np.zeros(image.shape[:2], np.uint8)
  cv2.circle(mask, (x, y), r, (255), 1)

  # add every point along circle to pixels_checked
  for pixel in cv2.findNonZero(mask):
    # this should be easier
    x = pixel[0, 0]
    y = pixel[0, 1]
    # fix weirdness
    if x < image.shape[0] and y < image.shape[1]:
      pixels_checked.append(image[x, y].tolist())

  pixels_checked = np.array(pixels_checked)

  channels = pixels_checked
  deviation = np.std(channels, axis=0)

  # eyeballed based on some test cases
  return (deviation < 20).all()

# pass it original circles, returns a filter to be used for finding circles already in the original (by checking if the center is within 50 pixels)
def circles_closeness_filter(circles, image):
  def filter(circle):
    x, y, r = circle
    isnt_duplicate = not any(map(lambda (xB, yB, _): (x - 25 <= xB <= x + 25) and (y - 25 <= yB <= y + 25), circles))
    correct_size = 50 <= r <= 100
    return isnt_duplicate and correct_size and ring_solid_color(circle, image)
  return filter

# filter out duplicate circles
filtered_circles = filter(circles_closeness_filter(circlesB, imageA), circlesA)

# draw filtered circles for debugging
if filtered_circles is not None:
  for (x, y, r) in filtered_circles:
    cv2.circle(outputA, (x, y), r, (0, 255, 0), 4)
    cv2.rectangle(outputA, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)
 
  # display for debugging
  cv2.imshow("outputA", outputA)
  cv2.imwrite("minis.png", outputA)
  cv2.waitKey(0)
